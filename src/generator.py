"""
Template generator core of python-boilerplate.com
"""
import os
import hashlib
import tempfile
import shutil
import json
import datetime
from itertools import combinations

from yapf import yapf_api
from jinja2 import Environment, FileSystemLoader

# User config strings, which come with the API call
BOILERPLATE_PYTHON2 = "py2"
BOILERPLATE_PYTHON3 = "py3"
BOILERPLATE_FLASK = "flask"
BOILERPLATE_IS_EXECUTABLE = "executable"
BOILERPLATE_INCLUDE_GITIGNORE = "gitignore"
BOILERPLATE_ARGPARSE = "argparse"
BOILERPLATE_USE_LOGGING = "logging"
BOILERPLATE_UNITTEST = "unittest"
BOILERPLATE_PYTEST = "pytest"
BOILERPLATE_TOX = "tox"
BOILERPLATE_INCLUDE_UTILS = "utils"

# Used only internally
BOILERPLATE_INCLUDE_REQUIREMENTS_TXT = "requirements_txt"

# possible for each py2 and py3
CONFIG_FLAGS = [
    BOILERPLATE_FLASK,
    BOILERPLATE_IS_EXECUTABLE,
    BOILERPLATE_INCLUDE_GITIGNORE,
    BOILERPLATE_ARGPARSE,
    BOILERPLATE_USE_LOGGING,
    BOILERPLATE_UNITTEST,
    BOILERPLATE_PYTEST,
    BOILERPLATE_TOX,
    # BOILERPLATE_INCLUDE_UTILS
]

# config combinations that will never be generated together
EXCLUSIVE_CONFIGS = [
    set([BOILERPLATE_UNITTEST, BOILERPLATE_PYTEST])
]

# configs which need special test commands (not 'python main.py dummyarg')
TEST_SPECIAL_COMMAND = {
    BOILERPLATE_FLASK: "exit 0"  # TODO: Only test if using py.test or unittest
}

# Style can be 'pep8', 'google', 'chromium', 'facebook' or a custom style
# See also:
# * https://github.com/google/yapf#formatting-style
# * https://github.com/google/yapf/blob/master/yapf/yapflib/style.py
DEFAULT_STYLE = "pep8"

here = os.path.abspath(os.path.dirname(__file__))
env = Environment(loader=FileSystemLoader(os.path.join(here, 'templates')))

# logger = logging.getLogger(__name__)


def log(s):
    """ Simple pseudo-logger which prepends the current isodate (useful instead of print since django) """
    now = datetime.datetime.now().isoformat()
    print("[%s] %s" % (now, s))
    # logger.info(s)


def get_possible_config_combinations():
    """ Returns all possible configuration combinations. """
    state_combinations = []
    for L in range(0, len(CONFIG_FLAGS)+1):
        for subset in combinations(CONFIG_FLAGS, L):
            # Check if exclusive configs are present in this subset
            _subset = set(subset)
            skip = any([exclusive_config <= _subset for exclusive_config in EXCLUSIVE_CONFIGS])
            if skip:
                # print("SKIP:", subset)
                continue
            state_combinations.append(("py2",) + subset)
            state_combinations.append(("py3",) + subset)
    return state_combinations


def generate_boilerplate_files(config_arr, style_config=DEFAULT_STYLE):
    """
    Generate the boilerplate files from the templates, based on an array
    of configuration flags as received from the API.
    """
    # Map config flags to constants for easy access from the code
    is_python2 = BOILERPLATE_PYTHON2 in config_arr
    is_python3 = not is_python2
    is_flask = BOILERPLATE_FLASK in config_arr
    is_executable = BOILERPLATE_IS_EXECUTABLE in config_arr
    is_using_argparse = BOILERPLATE_ARGPARSE in config_arr
    is_using_logging = BOILERPLATE_USE_LOGGING in config_arr
    is_using_unittest = BOILERPLATE_UNITTEST in config_arr
    is_using_pytest = BOILERPLATE_PYTEST in config_arr
    is_using_tox = BOILERPLATE_TOX in config_arr
    is_using_utils = BOILERPLATE_INCLUDE_UTILS in config_arr
    is_using_gitignore = BOILERPLATE_INCLUDE_GITIGNORE in config_arr

    # Whether to include requirements.txt
    include_requirements_txt = any([
        is_flask,
        is_using_pytest,
        is_using_tox,
        is_using_logging
    ])

    # Map config flags to a dictionary for easy access from the templates
    template_context = {
        BOILERPLATE_PYTHON2: is_python2,
        BOILERPLATE_PYTHON3: is_python3,
        BOILERPLATE_FLASK: is_flask,
        BOILERPLATE_IS_EXECUTABLE: is_executable,
        BOILERPLATE_ARGPARSE: is_using_argparse,
        BOILERPLATE_USE_LOGGING: is_using_logging,
        BOILERPLATE_UNITTEST: is_using_unittest,
        BOILERPLATE_PYTEST: is_using_pytest,
        BOILERPLATE_TOX: is_using_tox,
        BOILERPLATE_INCLUDE_UTILS: is_using_utils,
        BOILERPLATE_INCLUDE_GITIGNORE: is_using_gitignore,
        BOILERPLATE_INCLUDE_REQUIREMENTS_TXT: include_requirements_txt
    }

    # log(template_context)

    files = []
    # Templates are included here so they are refreshed on every request (without server restart)
    # First come the templates which work for both Python 2 and 3:
    # template_logger = env.get_template("logger.py.jinja2")
    template_utils = env.get_template("utils.py.jinja2")
    template_unittest = env.get_template("unittest.py.jinja2")
    template_unittest_flask = env.get_template("unittest_flask.py.jinja2")
    template_pytest = env.get_template("pytest.py.jinja2")
    template_pytest_flask = env.get_template("pytest_flask.py.jinja2")
    template_tox = env.get_template("tox.ini.jinja2")
    template_requirements = env.get_template("requirements.txt.jinja2")

    # Then come the Python-version specific files
    if is_flask:
        template_main = env.get_template("main_flask.py.jinja2")

    elif is_python2:
        template_main = env.get_template("main_py2.py.jinja2")

    else:
        template_main = env.get_template("main_py3.py.jinja2")

    # main.py
    main_py = yapf_api.FormatCode(
        template_main.render(template_context),
        style_config=style_config)[0]
    files.append({"name": "main.py", "content": main_py})

    if is_using_logging:
        # # logger.py
        # logger_py = yapf_api.FormatCode(
        #     template_logger.render(template_context),
        #     style_config=style_config)[0]
        # files.append({"name": "logger.py", "content": logger_py})
        include_requirements_txt = True

    if is_using_unittest:
        # tests.py
        if is_flask:
            code = yapf_api.FormatCode(
                template_unittest_flask.render(template_context),
                style_config=style_config)[0]
        else:
            code = yapf_api.FormatCode(
                template_unittest.render(template_context),
                style_config=style_config)[0]
        files.append({"name": "tests.py", "content": code})

    if is_using_pytest:
        if is_flask:
            # test_flask.py
            code = yapf_api.FormatCode(
                template_pytest_flask.render(template_context),
                style_config=style_config)[0]
            files.append({"name": "test_flask.py", "content": code})
        else:
            # test_sample.py
            code = yapf_api.FormatCode(
                template_pytest.render(template_context),
                style_config=style_config)[0]
            files.append({"name": "test_sample.py", "content": code})

    if is_using_tox:
        # tox.ini
        content = template_tox.render(template_context)
        files.append({"name": "tox.ini", "content": content})

    if include_requirements_txt:
        # requirements.txt
        content_raw = template_requirements.render(template_context)
        # remove empty lines
        content = "\n".join([line for line in content_raw.split("\n") if line.strip()])
        files.append({"name": "requirements.txt", "content": content})

    if is_using_utils:
        # utils.py
        utils_py = yapf_api.FormatCode(
            template_utils.render(template_context),
            style_config=style_config)[0]
        files.append({"name": "utils.py", "content": utils_py})

    if is_using_gitignore:
        # .gitignore
        filename = os.path.join(here, "templates", "Python.gitignore")
        content = open(filename).read()
        files.append({"name": ".gitignore", "content": content})

    return files


def generate_boilerplate_zip(config_arr, dir_out="/tmp/boilerplates/", filename=None):
    """
    Generates a zip archive of the boilerplate files and moves it
    to /tmp/boilerplates/<hash>.zip

    If filename == None, filename is a md5 hash of the files
    """
    # generate boilerplate
    files = generate_boilerplate_files(config_arr)

    # generate state hash
    md5 = hashlib.md5(json.dumps(files).encode('utf-8')).hexdigest()

    # build zip filename
    _fn = filename or "%s.zip" % md5
    zip_fn = os.path.join(dir_out, _fn)

    # log("generating zip file in %s" % zip_fn)
    if os.path.isfile(zip_fn) and not os.getenv("SKIP_ZIP_CACHE"):
        log("zip file %s already exists; not recreating." % zip_fn)
        return files, zip_fn, md5

    # All zips will be put into /tmp/boilerplates/
    if not os.path.exists(dir_out):
        os.mkdir(dir_out)

    # Create the zip file in a temp directory
    rootpath = tempfile.mkdtemp()
    outpath = os.path.join(rootpath, "boilerplate")
    os.mkdir(outpath)

    # Write every file content into a file
    for entry in files:
        fn = os.path.join(outpath, entry["name"])
        with open(fn, "w") as f:
            f.write(entry["content"])

    if "executable" in config_arr:
        #log("making main.py executable")
        os.system("cd %s/boilerplate && chmod +x main.py" % (rootpath))

    # Zip it
    zip_cmd = "7z a boilerplate.zip boilerplate > /dev/null"
    os.system("cd %s && %s && mv boilerplate.zip %s" % (rootpath, zip_cmd, zip_fn))

    # Remote tempfiles
    shutil.rmtree(rootpath)

    return files, zip_fn, md5


if __name__ == "__main__":
    print(get_possible_config_combinations())
